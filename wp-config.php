<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'phpapptest' );

/** MySQL hostname */
define( 'DB_HOST', 'dbhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dO&@{7+~w?fd-;Oec>G3YQ^tz6-i-([9p]{kdD@][GY*c@|3+:P|d1.-!0fVv-EU');
define('SECURE_AUTH_KEY',  '3wb7uElKh:Z|ag-t=X,[%g1,^GJyv|FR{hE6|4iX-CirZ;KQ%kxU6Y1k{*Lm~c3v');
define('LOGGED_IN_KEY',    'q*ht@38n= _LK9wRAENbxKtN#8@S~`t l$B-+A$nSnRs%(LYes0G+Sj:iQ$Z+5_R');
define('NONCE_KEY',        'hD<jFK p%WbwtM^};^vk*}5+]K~q/Y4_|)61%IR.|4_ETJfQckOGRF]of)*qZN-A');
define('AUTH_SALT',        'W|,+HG)Fjh$-mS[2qfU[fEEw3a;~<r=9{&5CvfD=i6] t[WTDeowtADIK0ji;+zY');
define('SECURE_AUTH_SALT', 'lA:{0-FpF8#lci-v^51B~{KcacWh[H%5?4$|AL6clk@l2?c0do(N2[!j(x-]tEwF');
define('LOGGED_IN_SALT',   'Zg+_/u+@XKosc+n2,~/ZW]bH250L(QD&3PL0|~6T%@gU,h>qUJ~w9fBF!-|B>O|;');
define('NONCE_SALT',       'vm8j$hBm/d/VKbGpze<WbAOdQ-5A)@oZ<L`$~ky1Q|m>u$j)bB/b1#L`f-Hk{u]H');
/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
